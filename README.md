# Bilemo Rest API

This project is a training project. 
It's the 7th Project of PHP / Symfony web developer cursus of OpenClassRooms 

This REST API offers consultation of a product catalog for its customers. These customers must be identified by a token. 
They also have the ability to manage their account informations and their own users.
Admin user can manage all products, clients and users.
This Api respect Richardon model, level 1,2 and 3.

The project also includes a ready to elvolve front part with only two pages (a home page and Api documentation).
The front final result is visible on [Bilemo Online](https://bilemo.oc-armellebraud.fr). 
Use Postman for visualize Bilemo Api possibilities.

## Requirements
* [MySQL 5.0.12](https://www.mysql.com/fr/)
* [PHP 7.3.6](http://php.net/manual/fr/intro-whatis.php)
* [Apache 2.4.38](https://www.apache.org/)
* Libraries PHP installed via [composer](https://getcomposer.org/download/)

##### Environment #####
* Symfony 5.0.5
* Composer 1.9.0

##### Bundles #####
  * LexikJWTAuthentification (JSON Token)
  * SwiftMailer (mailer)
  * BazingaHateoasBundle (discoverability)
  * elmioApiDocBundle 5Documentation generator)
  * fzaninotto Faker - dev only (tools for fake data)
  
##### PHP Library #####
  * behat/transliterator 1.3 (dev only)
  * Font-awesome 5.9.0

## Installation 
* Clone the repository
```
  git clone https://gitlab.com/armelle-formation/bilemo.git
```

* Install composer on your server following "Command-line installation" here => https://getcomposer.org/download/

* Install dependencies.
```
  composer install
```
## Configuration
* Duplicate the .env.dist file and customize it with your database and email identifiers
```
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"`
```
* Rename this file in .env and upload it on your server

## LexikJWTAuthenticationBundle
1. Generate the SSH keys
```
$ mkdir config/jwt
$ openssl genrsa -out config/jwt/private.pem -aes256 4096
$ openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
```
2. Copy ytour passephrase into .env file

## Database
Import bilemo.sql file in your database 

Your can also generate fake content (warning : you must be in dev environnement : remember to update your .env file)
* Create database 
```
  php bin/console doctrine:database:create
```
* Get tables 
```
  php bin/console doctrine:migrations:migrate
```
* Generate content
```
  php bin/console doctrine:fixtures:load  
```

## Access
Whether you use the .sql file or fixtures, an admin account is already created.
If you keep using this sources, create new account and delete this one

## Usage

1. Authentication Login link : [https://bilemo.oc-armellebraud.fr/api/login](https://bilemo.oc-armellebraud.fr/api/login)

Enter admin demo account credentials in body request :
```
{
   "email" : "admin@bilemo.fr",
   "password" : "password"
}
```
2. Enter the url of your choice to query the product catalog or manage users (see [documentation](https://bilemo.oc-armellebraud.fr/doc))
3. Don't forget to specify the token provided in step 1 (token is a Bearer Token type) for all your requests


## Documentation
Consult [Full documentation online](https://bilemo.oc-armellebraud.fr/doc)

## Code Quality
Medal A on SonarQube (HTML, CSS ans PHP PSR-2)  
![alt text](public/assets/img/quality_gate.jpg)

## Project status
Development has slowed down but not stopped. You can contact me if you need to help or more informations

## Copyright and License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.
