-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  Dim 05 avr. 2020 à 18:40
-- Version du serveur :  10.2.31-MariaDB
-- Version de PHP :  7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ocarugfu_bilemo`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp(),
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `name`, `roles`, `password`, `email`, `date_create`, `date_update`, `is_active`) VALUES
(1, 'Admin', '[\"ROLE_ADMIN\"]', '$argon2i$v=19$m=65536,t=4,p=1$VTV1QlFJWHlKbDk2Yzk3Zw$vIel1spd5rm3KYQaJcIByMy6oj/CS3BHQfimLDvCsRc', 'admin@bilemo.fr', '1987-02-08 16:09:23', '2020-03-30 18:15:43', 1),
(2, 'Vaillant', '[\"ROLE_USER\"]', '$argon2i$v=19$m=65536,t=4,p=1$NWo5U2tWYnQvd3Myc3ZKVw$CuyNM4cyimUBuQG0tyVXRP3xp28h80lyiKxT9jGV6WY', 'wlemaitre@vaillant.net', '2020-02-20 06:58:26', '2020-03-30 18:15:44', 1),
(3, 'Delorme', '[\"ROLE_USER\"]', '$argon2i$v=19$m=65536,t=4,p=1$YWdZZ3J5My9kZjBaMGdQSA$nempQ9uL0FNIjHoBgnm4oMO+ra23HIS5BaGwkBGzZlo', 'noel.tristan@delorme.fr', '2019-12-05 07:39:28', '2020-03-30 18:15:44', 1),
(4, 'Roux', '[\"ROLE_USER\"]', '$argon2i$v=19$m=65536,t=4,p=1$SXZKUzZZNGdxWDJ0bUw1aQ$uIAl7Z0eNt2q+6qbIekGFZ+US5h6GELsWrjNUTLIRjo', 'raymond.petit@roux.com', '2019-12-06 22:42:24', '2020-03-30 18:15:45', 0),
(5, 'Lelievre', '[\"ROLE_USER\"]', '$argon2i$v=19$m=65536,t=4,p=1$bDIxRlJKLy5zRG0xVTd5Mw$96abuJT1qvGL1SPE8hP2ZaBCJxyFj3NJc95FoOOpDjE', 'leroux.helene@lelievre.com', '2020-02-17 05:03:43', '2020-03-30 18:15:46', 1);

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `date_release` datetime DEFAULT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp(),
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `brand`, `model`, `color`, `stock`, `date_release`, `date_create`, `date_update`, `reference`, `price`, `currency`, `description`) VALUES
(1, 'Vivo', 'Z6 5G', 'gray', 2, '2019-07-05 00:49:39', '2019-11-24 11:43:09', '2020-03-30 18:15:47', '55600354', 770.94, 'EUR', 'Des argenteries reluisaient aux boutiques des orfèvres, et la basse-taille du ministre de l\'intérieur.'),
(2, 'Apple', 'iPhone', 'blue', 31, '2018-11-12 20:35:49', '2020-02-14 00:09:51', '2020-03-30 18:15:47', '27067949', 683.81, 'EUR', 'M. Lheureux, à défaut d\'autres, lui prendrait certainement. Elle s\'acheta des bottes vernies, il prit ses sabots à ses bandeaux lisses.'),
(3, 'Xiaomi', 'Redmi 8A Dual', 'teal', 42, '2019-02-12 20:40:25', '2020-03-05 13:18:39', '2020-03-30 18:15:47', '39713759', 1185.7, 'EUR', 'La chambre, au rez-de-chaussée, la seule qui me fait rien du tout; mais l\'idée seulement du mien qui coule suffirait à me torturer. Je n\'y tiens plus! Sauve-moi! Elle se leva (la.'),
(4, 'Nubia', 'X', 'black', 15, '2017-12-07 18:22:06', '2019-10-24 15:54:30', '2020-03-30 18:15:47', '51843199', 913.13, 'EUR', 'Elle resta jusqu\'au soir à l\'église. M. Bournisien dans le coin, à gauche, sur la vie, cette vie luxueuse qu\'il lui fallait une femme. Elle lui donna un grand amusement que de.'),
(5, 'Huawei', 'Enjoy 10e', 'fuchsia', 40, '2019-09-12 21:26:51', '2019-11-17 04:11:16', '2020-03-30 18:15:47', '51212407', 1255.86, 'EUR', 'Elle avait fait des lacs de confitures dans une boutique, je restais dans la rue, je vous ai bien aimée! Et.'),
(6, 'Nubia', 'Z18', 'yellow', 6, '2019-10-22 19:55:24', '2019-08-14 19:24:49', '2020-03-30 18:15:47', '86620642', 271.73, 'EUR', 'Ensuite il fallait courir aux leçons, à l\'amphithéâtre, à l\'hospice, et revenir chez lui, près de lui, sur le péristyle de la Fresnaye, tué à la vouloir posséder. D\'ailleurs, sa timidité s\'était.'),
(7, 'Nubia', 'Z20', 'aqua', 21, '2019-05-11 14:45:41', '2020-01-29 11:50:38', '2020-03-30 18:15:47', '06540111', 236.03, 'EUR', 'À table même, elle apportait son livre, et elle faisait fondre des morceaux de sucre, près d\'une.'),
(8, 'Huawei', 'Y7p', 'aqua', 27, '2020-01-16 15:25:30', '2019-11-12 07:35:59', '2020-03-30 18:15:47', '84915757', 1272.01, 'EUR', 'Elle monta le large escalier droit, et notamment par la saisie exécutoire de ses adultères et de petites chambres à la foire.'),
(9, 'Vivo', 'Nex 3S 5G', 'navy', 45, '2018-12-23 00:26:54', '2019-06-01 12:09:25', '2020-03-30 18:15:47', '00584111', 858.19, 'EUR', 'Elle retira les globes des flambeaux, fit coller des papiers neufs, repeindre l\'escalier et faire des préparatifs de la suivre, envoya chercher des cigares l\'étourdit.'),
(10, 'Xiaomi', 'Redmi Note 9 Pro Max', 'purple', 43, '2018-10-31 08:11:30', '2019-12-09 11:43:38', '2020-03-30 18:15:47', '61230866', 865.5, 'EUR', 'De cette tragédie, par exemple, ne voudrait-il pas débarrasser ce pauvre coeur, douce et indistincte, comme le.');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `client_id`, `firstname`, `lastname`, `email`, `date_create`, `date_update`) VALUES
(1, 4, 'Michelle', 'Fischer', 'michelle.fischer@roux.com', '2019-08-01 15:43:45', '2020-03-30 18:15:46'),
(2, 4, 'Marcel', 'Roche', 'marcel.roche@roux.fr', '2019-05-14 18:18:54', '2020-03-30 18:15:46'),
(3, 5, 'Émilie', 'Pichon', 'emilie.pichon@lelievre.fr', '2020-02-14 15:08:35', '2020-03-30 18:15:46'),
(4, 5, 'Marie', 'Thibault', 'marie.thibault@lelievre.org', '2019-03-31 03:25:37', '2020-03-30 18:15:46'),
(5, 3, 'Capucine', 'Camus', 'capucine.camus@delorme.fr', '2019-10-26 05:24:08', '2020-03-30 18:15:46'),
(6, 2, 'Caroline', 'Sauvage', 'caroline.sauvage@vaillant.fr', '2019-12-03 05:49:25', '2020-03-30 18:15:46'),
(7, 2, 'Danielle', 'Lebrun', 'danielle.lebrun@vaillant.org', '2020-01-02 19:20:11', '2020-03-30 18:15:46'),
(8, 5, 'Maryse', 'Renaud', 'maryse.renaud@lelievre.fr', '2019-08-06 07:19:28', '2020-03-30 18:15:46'),
(9, 4, 'Denis', 'Dupre', 'denis.dupre@roux.com', '2019-09-21 09:49:09', '2020-03-30 18:15:46'),
(10, 3, 'Laurent', 'Benoit', 'laurent.benoit@delorme.fr', '2019-04-12 04:06:39', '2020-03-30 18:15:46'),
(11, 2, 'Élisabeth', 'Charpentier', 'elisabeth.charpentier@vaillant.com', '2019-07-18 04:48:52', '2020-03-30 18:15:46'),
(15, 3, 'Michel', 'Deschamps', 'michel.deschamps@delorme.com', '2019-12-25 09:02:26', '2020-03-30 18:15:46'),
(16, 3, 'Henri', 'Richard', 'henri.richard@delorme.com', '2020-01-01 12:15:29', '2020-03-30 18:15:46'),
(17, 4, 'Honoré', 'Tessier', 'honore.tessier@roux.com', '2019-12-08 10:26:48', '2020-03-30 18:15:46'),
(18, 3, 'Andrée', 'Aubert', 'andree.aubert@delorme.fr', '2019-09-15 02:57:44', '2020-03-30 18:15:46'),
(19, 2, 'Stéphane', 'Riviere', 'stephane.riviere@vaillant.fr', '2019-06-16 19:19:11', '2020-03-30 18:15:46');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C74404555E237E06` (`name`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8D93D64919EB6921` (`client_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D64919EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
