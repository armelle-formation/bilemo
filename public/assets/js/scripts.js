(function($) {
    "use strict"; 

    /* Rotating Text - Morphtext */
	$("#js-rotating").Morphext({
		animation: "fadeIn",
		separator: ",",
		speed: 2000,
		complete: function () {
		}
    });
    

})(jQuery);