<?php

namespace App\Controller;

use Exception;
use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;

class AccountController extends AbstractController
{
    /**
    * Send confirm email for registration
    * @Route(path="/activate/{token}", format="html", name="confirm_register")
    */
    public function confirmRegister(ClientRepository $clientRepo, $token, JWTEncoderInterface $jwtEncoder, EntityManagerInterface $manager)
    {
        try {
            //Check if token exists
            if (!$token) {
                throw $this->createNotFoundException();
            }
            
            //Decode token
            $data = $jwtEncoder->decode($token);
            
            //Check if user exists
            $client = $clientRepo->find($data['id']);
            if (!$client instanceof Client) {
                throw new AccessDeniedHttpException();
            }
            
            //Check email corresponding
            if ($data['email'] !== $client->getEmail()) {
                throw new AccessDeniedHttpException();
            }
            
            //Active and save account
            $client->setIsActive(1);
            
            //Update user account
            $manager->persist($client);
            $manager->flush();
            
            //Message for user
            $this->addFlash('success', "Votre compte a bien été activé. Vous pouvez désormais utiliser l'API Bilemo");
        } catch (JWTDecodeFailureException $e) {
            $this->addFlash(
                'warning',
                "Le lien de réinitialisation est erroné ou a expiré. Merci de refaire une demande."
            );
        } catch (Exception $e) {
            throw new AccessDeniedHttpException();
        }

        //Redirect on activate page
        return $this->render('security/activate.html.twig');
    }
}
