<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Service\DbService;
use App\Service\FormErrors;
use App\Service\Pagination;
use App\Service\EmailSender;
use App\Service\TokenGenerate;
use Swagger\Annotations as SWG;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use JMS\Serializer\DeserializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Nelmio\ApiDocBundle\Annotation\Security as nSecurity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use JMS\Serializer\SerializerInterface as SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
* Class ClientController
* @Route("/api", name="clients_", defaults={"_format"="json"})
*/
class ClientController extends AbstractController
{
    /**
    * Show a list of all clients. Paged by 10, order by id ASC - Admin only
    * @Route("/admin/clients", name="list", methods={"GET"})
    *  @SWG\Parameter(
    *   name="page",
    *   description="The page number to show",
    *   in="query",
    *   type="integer"
    * )
    * @SWG\Parameter(
    *   name="limit",
    *   description="The number of Client per page",
    *   in="query",
    *   type="integer"
    * )
    * @SWG\Parameter(
    *   name="orderBy",
    *   description="Sorts Clients based on specified field",
    *   in="query",
    *   type="string"
    * )
    * @SWG\Parameter(
    *   name="sort",
    *   description="Direction for specified sort field",
    *   in="query",
    *   type="string"
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *      @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=Client::class))
    *     )
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @nSecurity(name="Bearer")
    * @SWG\Tag(name="Client")
    * @param SerializerInterface $serializer
    * @param Request $request
    * @param Pagination $pagination
    * @return JsonResponse
    * @throws \Doctrine\DBAL\DBALException
    */
    public function getClients(SerializerInterface $serializer, Request $request, Pagination $pagination, DbService $dbService) : JsonResponse
    {
        // Cache
        $response = new JsonResponse();
        $response->setEtag(md5($dbService->getLastUpdate('client')));
        $response->headers->addCacheControlDirective('no-control');
        $response->setPublic();

        if ($response->isNotModified($request)) {
            return $response;
        }

        // Parameters
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', getenv('LIMIT'));
        $orderby = $request->query->get('orderby', getenv('ORDERBY'));
        $sort = $request->query->get('sort', getenv('SORT'));
        
        // Data
        $pagination->setEntityClass(Client::class)
            ->setPage($page)
            ->setMethod('findAllClientsWithoutAdmin')
            ->setSort($sort)
            ->setlimit($limit)
            ->setOrderBy($orderby);
        
        $context = SerializationContext::create()->setGroups(array('Default'));
        $data = $serializer->serialize($pagination->getData(), 'json', $context);
        $response->setJson($data, Response::HTTP_OK, [], true);

        return $response;
    }
    
    /**
    * Get details about a specific client - Admin only
    * @Route("/admin/clients/{id}", name="show", methods={"GET"})
    * @SWG\Parameter(
    *   name="id",
    *   description="Client id",
    *   in="path",
    *   required=true,
    *   type="integer"
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *     @SWG\Schema(
    *          type="array",
    *         @SWG\Items(ref=@Model(type=Client::class))
    *     )
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="Client")
    * @nSecurity(name="Bearer")
    * @Security(
    *   "(user === clientToSee && clientToSee.getIsActive()) || is_granted('ROLE_ADMIN')",
    *    message="Vous ne pouvez visualiser que vos propres informations"
    * )
    *
    * @param Client $clientToSee
    * @param SerializerInterface $serializer
    * @return JsonResponse
    */
    public function showClient(Client $clientToSee, SerializerInterface $serializer) : JsonResponse
    {
        $context = SerializationContext::create()->setGroups(array('Default','show', 'embeddedUsers'));
        $data = $serializer->serialize($clientToSee, 'json', $context);
        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }
    
    /**
    * Get details about connected client
    * @Route("/clients/me", name="show_me", methods={"GET"})
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *     @SWG\Schema(
    *          type="array",
    *         @SWG\Items(ref=@Model(type=Client::class))
    *     )
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="Client")
    * @nSecurity(name="Bearer")
    * @param SerializerInterface $serializer
    * @return JsonResponse
    */
    public function showCurrentClient(SerializerInterface $serializer) : JsonResponse
    {
        $context = SerializationContext::create()->setGroups(array('Default','show', 'embeddedUsers'));
        $data = $serializer->serialize($this->getUser(), 'json', $context);
        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }

    /**
    * Create a client - Admin only
    * @Route("/admin/clients", name="create", methods={"POST"})
    * @SWG\Parameter(
    *   name="Client",
    *   description="Fields to provide for create a new Client",
    *   in="body",
    *   required=true,
    *   type="string",
    *   @SWG\Schema(
    *       title="Client fields",
    *       required={"name", "email", "password"},
    *       @SWG\Property(property="name", type="string", example="Promo Phones"),
    *       @SWG\Property(property="email", type="string", example="contact@promophones.fr"),
    *       @SWG\Property(property="password", type="string", example="Password#123", description="Must contain at least one lowercase, one uppercase, one digit and one special character"),
    *   )
    * )
    * @SWG\Response(
    *     response=201,
    *     description="CREATED",
    *     @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=Client::class))
    *     )
    * )
    * @SWG\Response(
    *     response=400,
    *     description="BAD REQUEST"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Tag(name="Client")
    * @nSecurity(name="Bearer")
    * @IsGranted("ROLE_ADMIN")
    * @param Request $request
    * @param UserPasswordEncoderInterface $passwordEncoder
    * @param SerializerInterface $serializer
    * @param EntityManagerInterface $entityManager
    * @param ValidatorInterface $validator
    * @param EmailSender $mailer
    * @param JWTEncoderInterface $jwtEncoder
    * @param TokenGenerate $tokenService
    * @param ParameterBagInterface $params
    * @return JsonResponse
    */
    public function create(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EmailSender $mailer,
        JWTEncoderInterface $jwtEncoder,
        TokenGenerate $tokenService,
        ParameterBagInterface $params
    ) : JsonResponse {
        $values = json_decode($request->getContent());
        
        if (!isset($values->email,$values->password,$values->name)) {
            $data = [
                'status' => 500,
                'message' => 'Vous devez renseigner les clés name, email et password'
            ];
            
            return new JsonResponse($serializer->serialize($data, 'json'), Response::HTTP_INTERNAL_SERVER_ERROR, [], true);
        }
            
        $context = DeserializationContext::create()->setGroups(array('write'));
        $client = $serializer->deserialize($request->getContent(), Client::class, 'json', $context);

        $errors = $validator->validate($client);
        
        if (count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new JsonResponse(
                $errors,
                400,
                [
                    'Content-Type' => 'application/json'
                ],
                true
            );
        }

        $password = $passwordEncoder->encodePassword($client, $client->getPassword());
        $client->setPassword($password);
        $client->setRoles($client->getRoles());

        $manager->persist($client);
        $manager->flush();

        //Generate token : jwt valid for 30mn from the issued time (by default)
        $token = $tokenService->generateTokenForUser($jwtEncoder, $client);

        //Send confirmation email with account activation page link
        $subject = 'Confirmation de création de compte';
        $content = $this->renderView(
            'emails/activation.html.twig',
            [
                'name' => $client->getName(),
                'id' => $client->getId(),
                'token' => $token
            ],
            'text/html'
        );

        $return = $mailer->sendMessage($subject, $content, $client->getEmail(), $params->get('email-contact'));

        // Personnalized return messages
        $message = $return['success'] ?
            "Le client a bien été créé, un email d'activation lui a été envoyé." :
            "Le client a bien été créé, mais l'email d'activation n'a pu être envoyé.";

        $return = [
            'message' => $message,
            'client' => $client
        ];
        $context = SerializationContext::create()->setGroups(array('Default','show', 'embeddedUsers'));
        $data = $serializer->serialize($return, 'json', $context);
        return new JsonResponse($data, JsonResponse::HTTP_CREATED, [], true);
    }
    
    /** Update a client (partial update authorized)
    * @Route("/clients/{id}", name="update", methods={"PATCH"})
    * @SWG\Parameter(
    *   name="id",
    *   description="Client id",
    *   in="path",
    *   required=true,
    *   type="integer"
    * )
    * @SWG\Parameter(
    *   name="Client",
    *   description="Fields to provide to update a client",
    *   in="body",
    *   required=true,
    *   type="string",
    *   @SWG\Schema(
    *       title="Client fields",
    *       @SWG\Property(property="name", type="string", example="Promo Phones"),
    *       @SWG\Property(property="email", type="string", example="john.doe@promophones.fr")
    *   )
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *      @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=Client::class))
    *     )
    * )
    * @SWG\Response(
    *     response=400,
    *     description="BAD REQUEST"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="Client")
    * @nSecurity(name="Bearer")
    * @param Client $clientToUpdate
    * @param Request $request
    * @param EntityManagerInterface $manager
    * @param UserPasswordEncoderInterface $encoder;
    * @param SerializerInterface $serializer
    * @return JsonResponse
    */
    public function update(
        Client $clientToUpdate,
        Request $request,
        EntityManagerInterface $manager,
        FormErrors $formsErrors,
        UserPasswordEncoderInterface $passwordEncoder,
        SerializerInterface $serializer
    ) : JsonResponse {
        $this->denyAccessUnlessGranted('UPDATE', $clientToUpdate, 'Vous ne pouvez mettre à jour que vos propres informations');

        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(ClientType::class, $clientToUpdate);
        $form->submit($data, false);
        
        if ($form->isSubmitted() && !$form->isValid()) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'errors' => $formsErrors->getErrorsFromForm($form)
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }


        // Disabling yourself is forbidden
        if ($this->getUser() == $clientToUpdate && (isset($data['isActive']) && !$data['isActive'])) {
            throw new AccessDeniedHttpException('Vous ne pouvez pas vous désactiver vous même');
        }

        $password = $passwordEncoder->encodePassword($clientToUpdate, $clientToUpdate->getPassword());
        $clientToUpdate->setPassword($password);

        $manager->flush();

        $return = [
            'message' => 'Le client a bien été mis à jour',
            'client' => $clientToUpdate
        ];
        $context = SerializationContext::create()->setGroups(array('Default','show', 'embeddedUsers'));
        $data = $serializer->serialize($return, 'json', $context);
        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }
    
    /**
    * Delete a client - Admin only
    * @Route("/admin/clients/{id}", name="delete", methods={"DELETE"})
    * @SWG\Response(
    *     response=204,
    *     description="NO CONTENT"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="Client")
    * @nSecurity(name="Bearer")
    * @IsGranted("ROLE_ADMIN")
    * @param Client $client
    * @param EntityManagerInterface $manager
    * @return JsonResponse
    */
    public function delete(Client $client, EntityManagerInterface $manager) : JsonResponse
    {
        $this->denyAccessUnlessGranted('DELETE', $client, 'Vous ne pouvez pas vous supprimer vous même');

        $manager->remove($client);
        $manager->flush();
        
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
