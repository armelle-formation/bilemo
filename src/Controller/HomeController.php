<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * Display main page of the site
     * @Route("/", format="html", name="index")
     */
    public function index()
    {
        return $this->render('home/index.html.twig');
    }
}
