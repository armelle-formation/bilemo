<?php

namespace App\Controller;

use Exception;
use App\Entity\Product;
use App\Form\ProductType;
use App\Service\DbService;
use App\Service\FormErrors;
use App\Service\Pagination;
use DateTimeInterface;
use Swagger\Annotations as SWG;
use Faker\Provider\zh_TW\DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use JMS\Serializer\DeserializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Security as nSecurity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use JMS\Serializer\SerializerInterface as SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class ProductController
 * @package App\Controller
 * @Route("/api/products", name="products_", defaults={"_format"="json"})
 */
class ProductController extends AbstractController
{
    /**
    * Search products by criteria (brand, model or keyword). Paged by 10, order by id ASC)
    * @Route("", name="list", methods={"GET"})
    *  @SWG\Parameter(
    *   name="page",
    *   description="The page number to show",
    *   in="query",
    *   type="integer"
    * )
    * @SWG\Parameter(
    *   name="limit",
    *   description="The number of product per page",
    *   in="query",
    *   type="integer"
    * )
    * @SWG\Parameter(
    *   name="keyword",
    *   description="The keyword to search in description, currency or reference field",
    *   in="query",
    *   type="string"
    * )
    * @SWG\Parameter(
    *   name="brand",
    *   description="The brand of product to search",
    *   in="query",
    *   type="string"
    * )
    * @SWG\Parameter(
    *   name="model",
    *   description="The model of product to search",
    *   in="query",
    *   type="string"
    * )
    * @SWG\Parameter(
    *   name="orderBy",
    *   description="Sorts products based on specified field",
    *   in="query",
    *   type="string"
    * )
    * @SWG\Parameter(
    *   name="sort",
    *   description="Direction for specified sort field",
    *   in="query",
    *   type="string"
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *      @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=Product::class))
    *     )
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @nSecurity(name="Bearer")
    * @SWG\Tag(name="Product")
    * @param SerializerInterface $serializer
    * @param Request $request
    * @param Pagination $pagination
    * @param integer $page
    * @param integer $limit
    * @param string $keyword
    * @param string $brand
    * @param string $model
    * @param string $orderby
    * @param string $sort
    * @return JsonResponse
    * @throws \Doctrine\DBAL\DBALException
    */
    public function getProducts(SerializerInterface $serializer, Request $request, Pagination $pagination, DbService $dbService) : JsonResponse
    {
        
        // Cache
        $response = new JsonResponse();
        $response->setEtag(md5($dbService->getLastUpdate('product')));
        $response->headers->addCacheControlDirective('no-control');
        $response->setPublic();

        if ($response->isNotModified($request)) {
            return $response;
        }
        
        // Parameters
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', getenv('LIMIT'));
        $keyword = $request->query->get('keyword', '');
        $brand = $request->query->get('brand', '');
        $model = $request->query->get('model', '');
        $orderby = $request->query->get('orderby', getenv('ORDERBY'));
        $sort = $request->query->get('sort', getenv('SORT'));

        // Data
        $pagination->setEntityClass(Product::class)
            ->setPage($page)
            ->setlimit($limit)
            ->setMethod('findAllByCriterias')
            ->setArguments(['keyword' => $keyword, 'brand' => $brand, 'model' => $model])
            ->setSort($sort)
            ->setOrderBy($orderby);
        
        $context = SerializationContext::create()->setGroups(array('Default'));
        $data = $serializer->serialize($pagination->getData(), 'json', $context);
        $response->setJson($data, Response::HTTP_OK, [], true);

        return $response;
    }

    /**
    * Get detail of product
    * @Route("/{id}", name="show", methods={"GET"})
    * @SWG\Parameter(
    *   name="id",
    *   description="Product id",
    *   in="path",
    *   required=true,
    *   type="integer"
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *     @SWG\Schema(
    *         type = "array",
    *         @SWG\Items(ref=@Model(type=Product::class))
    *     )
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="Product")
    * @nSecurity(name="Bearer")
    * @Cache(lastModified="product.getDateUpdate()", Etag="'Product' ~ product.getId() ~ product.getDateUpdate().getTimestamp()", public=true)
    * @param Product $product
    * @param SerializerInterface $serializer
    * @return JsonResponse
    */
    public function showProduct(Product $product, SerializerInterface $serializer)
    {
        $data = $serializer->serialize($product, 'json');
        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }

    /**
    * Create a product - Admin only
    * @Route("", name="create", methods={"POST"})
    * @SWG\Parameter(
    *   name="Product",
    *   description="Fields to provide to create a new product",
    *   in="body",
    *   required=true,
    *   type="string",
    *   @SWG\Schema(
    *       title="Product fields",
    *       required={"brand", "model", "reference", "dateRelease"},
    *       @SWG\Property(property="brand", type="string", example="Apple"),
    *       @SWG\Property(property="model", type="string", example="Iphone X"),
    *       @SWG\Property(property="color", type="string", example="Red"),
    *       @SWG\Property(property="stock", type="integer", example="125"),
    *       @SWG\Property(property="dateRelease", type="string", example="2018-12-15"),
    *       @SWG\Property(property="reference", type="string", example="AB156-458", description="max length : 15 car."),
    *       @SWG\Property(property="price", type="double", example="725.53", description="Required if a currency is given."),
    *       @SWG\Property(property="currency", type="string", example="EUR", description="Must comply with ISO-4217 standard. Required if price is given."),
    *       @SWG\Property(property="description", type="string", example="Un téléphone dernière génération à la qualité éprouvée")
    *     )
    * )
    * @SWG\Response(
    *     response=201,
    *     description="CREATED",
    *     @SWG\Schema(
    *         @SWG\Items(ref=@Model(type=Product::class))
    *     )
    * )
    * @SWG\Response(
    *     response=400,
    *     description="BAD REQUEST"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Tag(name="Product")
    * @nSecurity(name="Bearer")
    * @IsGranted("ROLE_ADMIN")
    * @param Request $request
    * @param SerializerInterface $serializer
    * @param EntityManagerInterface $entityManager
    * @param ValidatorInterface $vaidator
    * @return JsonResponse
    */
    public function create(Request $request, EntityManagerInterface $manager, SerializerInterface $serializer, ValidatorInterface $validator) : JsonResponse
    {
        $values = json_decode($request->getContent());

        // Mandatory values have not been passed
        if (!isset($values->brand,$values->model,$values->reference)) {
            $data = [
                'status' => 500,
                'message' => 'Vous devez renseigner les clés brand, model et la reference'
            ];
            
            return new JsonResponse($serializer->serialize($data, 'json'), Response::HTTP_INTERNAL_SERVER_ERROR, [], true);
        }

        $context = DeserializationContext::create()->setGroups(array('write'));
        $product = $serializer->deserialize($request->getContent(), Product::class, 'json', $context);

        if (!empty($values->dateRelease)) {
            $product->SetDateRelease(new \DateTime($values->dateRelease));
        }
        $errors = $validator->validate($product);
        
        if (count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new JsonResponse(
                $errors,
                400,
                [
                    'Content-Type' => 'application/json'
                ],
                true
            );
        }
        
        $manager->persist($product);
        $manager->flush();
        $return = [
            'message' => 'Le produit a bien été ajouté',
            'product' => $product
        ];
        
        $context = SerializationContext::create()->setGroups(array('show'));
        $data = $serializer->serialize($return, 'json', $context);
        return new JsonResponse($data, JsonResponse::HTTP_CREATED, [], true);
    }

    /** Update a product (partial update authorized) - Admin only
    * @Route("/{id}", name="update", methods={"PATCH"})
    * @SWG\Parameter(
    *   name="id",
    *   description="Product id",
    *   in="path",
    *   required=true,
    *   type="integer"
    * )
    * @SWG\Parameter(
    *   name="Product",
    *   description="Fields to provide to update a product",
    *   in="body",
    *   required=true,
    *   type="string",
    *   @SWG\Schema(
    *       title="Product fields",
    *       @SWG\Property(property="brand", type="string", example="Apple"),
    *       @SWG\Property(property="model", type="string", example="Iphone X"),
    *       @SWG\Property(property="color", type="string", example="Red"),
    *       @SWG\Property(property="stock", type="integer", example="125"),
    *       @SWG\Property(property="dateRelease", type="string", example="2018-12-15"),
    *       @SWG\Property(property="reference", type="string", example="AB156-458", description="max length : 15 car."),
    *       @SWG\Property(property="price", type="double", example="725.53", description="Required if a currency is given."),
    *       @SWG\Property(property="currency", type="string", example="EUR", description="Must comply with ISO-4217 standard. Required if price is given."),
    *       @SWG\Property(property="description", type="string", example="Un téléphone dernière génération à la qualité éprouvée")
    *     )
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *     @Model(type=Product::class)
    * )
    * @SWG\Response(
    *     response=400,
    *     description="BAD REQUEST"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="Product")
    * @nSecurity(name="Bearer")
    * @IsGranted("ROLE_ADMIN")
    * @param Product $product
    * @param Request $request
    * @param EntityManagerInterface $manager
    * @param SerializerInterface $serializer
    * @return JsonResponse
    */
    public function update(Product $product, Request $request, EntityManagerInterface $manager, FormErrors $formsErrors, SerializerInterface $serializer) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(ProductType::class, $product);
        $form->submit($data, false);

        if ($form->isSubmitted() && !$form->isValid()) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'errors' => $formsErrors->getErrorsFromForm($form)
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $manager->persist($product);
        $manager->flush();

        $return = [
            'message' => 'Le produit a bien été mis à jour',
            'product' => $product
        ];
  
        $context = SerializationContext::create()->setGroups(array('show'));
        $data = $serializer->serialize($return, 'json', $context);
        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }

    /**
    * Delete a product - Admin only
    * @Route("/{id}", name="delete", methods={"DELETE"})
    * @SWG\Response(
    *     response=204,
    *     description="NO CONTENT"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="Product")
    * @nSecurity(name="Bearer")
    * @IsGranted("ROLE_ADMIN")
    * @param Product $product
    * @param EntityManagerInterface $manager
    * @return JsonResponse
    */
    public function delete(Product $product, EntityManagerInterface $manager) : JsonResponse
    {
        $manager->remove($product);
        $manager->flush();
        
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
