<?php

namespace App\Controller;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security as nSecurity;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
* @Route("/api")
*/
class SecurityController extends AbstractController
{
    
    /**
    * @Route("/login", name="login", methods={"POST"})
    * @SWG\Parameter(
    *   name="authToken",
    *   description="Fields to fill to sign in and get an AuthToken",
    *   in="body",
    *   required=true,
    *   type="string",
    *   @SWG\Schema(
    *     type="object",
    *     title="Authentication field",
    *     @SWG\Property(property="email", type="string"),
    *     @SWG\Property(property="password", type="string")
    *     )
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *    @SWG\Schema(
    *      type="string",
    *      title="Token",
    *      @SWG\Property(property="token", type="string"),
    *     )
    * )
    * @SWG\Response(
    *     response=400,
    *     description="Bad request - Invalid JSON",
    * )
    * @SWG\Response(
    *     response=401,
    *     description="Bad credentials"
    * )
    * @SWG\Tag(name="Authentication")
    * @return JsonResponse
    */
    public function login() {}

}
