<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\DbService;
use App\Service\FormErrors;
use App\Service\Pagination;
use Swagger\Annotations as SWG;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use JMS\Serializer\DeserializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Security as nSecurity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use JMS\Serializer\SerializerInterface as SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class UserController
 * @Route("/api", name="users_", defaults={"_format"="json"})
 */
class UserController extends AbstractController
{
    /**
     * Get list of all users - Admin only (default paged by 10, sort dateCreate DESC)
     * @Route("/admin/users", name="list", methods={"GET"})
    * @SWG\Response(
     *     response=200,
     *     description="OK",
     *      @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
     * )
     * @SWG\Tag(name="User")
     * @param SerializerInterface $serializer
     * @param Request $request
     * @param Pagination $pagination
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
    */
    public function getUsers(SerializerInterface $serializer, Request $request, Pagination $pagination, DbService $dbService) : JsonResponse
    {
        // Cache
        $response = new JsonResponse();
        $response->setEtag(md5($dbService->getLastUpdate('user')));
        $response->headers->addCacheControlDirective('no-control');
        $response->setPublic();

        if ($response->isNotModified($request)) {
            return $response;
        }

        // Parameters
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', getenv('LIMIT'));
        $orderby = $request->query->get('orderby', 'dateCreate');
        $sort = $request->query->get('sort', 'desc');
        
        // Data
        $pagination->setEntityClass(User::class)
            ->setPage($page)
            ->setSort($sort)
            ->setlimit($limit)
            ->setOrderBy($orderby);

        $context = SerializationContext::create()->setGroups(array('Default','show'));
        $data = $serializer->serialize($pagination->getData(), 'json', $context);
        $response->setJson($data, Response::HTTP_OK, [], true);

        return $response;
    }

    /**
    * Get list of users of a connected client(default paged by 10, sort dateCreate DESC)
    * @Route("/users", name="list_client", methods={"GET"})
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *      @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=User::class))
    *     )
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Tag(name="User")
    * @param SerializerInterface $serializer
    * @param Request $request
    * @param Pagination $pagination
    * @return JsonResponse
    *
    */
    public function listUserClient(SerializerInterface $serializer, Request $request, Pagination $pagination, DbService $dbService) : JsonResponse
    {
        // Cache
        $response = new JsonResponse();
        $response->setEtag(md5($dbService->getLastUpdate('user')));
        $response->headers->addCacheControlDirective('no-control');
        $response->setPublic();

        if ($response->isNotModified($request)) {
            return $response;
        }

        // Parameters
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', getenv('LIMIT'));
        $orderby = $request->query->get('orderby', 'dateCreate');
        $sort = $request->query->get('sort', 'desc');

        // Data
        $pagination->setEntityClass(User::class)
        ->setPage($page)
        ->setMethod('findAllUsersByClient')
        ->setArguments(['client' => $this->getUser()])
        ->setSort($sort)
        ->setlimit($limit)
        ->setOrderBy($orderby);

        $context = SerializationContext::create()->setGroups(array('Default'));
        $data = $serializer->serialize($pagination->getData(), 'json', $context);
        $response->setJson($data, Response::HTTP_OK, [], true);

        return $response;
    }

    /**
    * Get details about a specific user
    * @Route("/users/{id}", name="show", methods={"GET"})
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *      @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=User::class))
    *     )
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="User")
    * @Security(
    *   "user === userToSee.getClient() || is_granted('ROLE_ADMIN')",
    *    message="Vous ne pouvez pas visualiser un utilisateur qui ne vous appartient pas"
    * )
    * @Cache(lastModified="userToSee.getDateUpdate()", Etag="'User' ~ userToSee.getId() ~ userToSee.getDateUpdate().getTimestamp()", public=true)
    * @param User $userToSee
    * @param SerializerInterface $serializer
    * @return JsonResponse
    */
    public function showUser(User $userToSee, SerializerInterface $serializer) : JsonResponse
    {
        $context = SerializationContext::create()->setGroups(array('Default','show'));
        $data = $serializer->serialize($userToSee, 'json', $context);
        
        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
    
    /**
    * Create a user for the connected client
    * @Route("/users", name="create", methods={"POST"})
    * @SWG\Parameter(
    *   name="User",
    *   description="Fields to provide for create a new User",
    *   in="body",
    *   required=true,
    *   type="string",
    *   @SWG\Schema(
    *       title="User fields",
    *       required={"firstname", "lastname", "email"},
    *       @SWG\Property(property="firstname", type="string", example="Jane"),
    *       @SWG\Property(property="lastname", type="string", example="Doe"),
    *       @SWG\Property(property="email", type="string", example="jane.doe@promophones.fr")
    *   )
    * )
    * @SWG\Response(
    *     response=201,
    *     description="CREATED",
    *     @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=User::class))
    *     )
    * )
    * @SWG\Response(
    *     response=400,
    *     description="BAD REQUEST"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Tag(name="User")
    * @nSecurity(name="Bearer")
    * @param Request $request
    * @param EntityManagerInterface $manager
    * @param SerializerInterface $serializer
    * @param ValidatorInterface $validator
    * @return JsonResponse
    */
    public function create(Request $request, EntityManagerInterface $manager, ClientRepository $clientRepo, SerializerInterface $serializer, ValidatorInterface $validator) : JsonResponse
    {
        $values = json_decode($request->getContent());

        if (!isset($values->email,$values->firstname,$values->lastname)) {
            $data = [
                'status' => 500,
                'message' => 'Vous devez renseigner les clés firstname, lastname et email'
            ];
            
            return new JsonResponse($serializer->serialize($data, 'json'), Response::HTTP_INTERNAL_SERVER_ERROR, [], true);
        }

        $context = DeserializationContext::create()->setGroups(array('write'));
        $user = $serializer->deserialize($request->getContent(), User::class, 'json', $context);
        
        // Client
        $client = $this->getUser();
        if (isset($values->idclient) && in_array("ROLE_ADMIN", $client->getRoles())) {
            $client = $clientRepo->find($values->idclient);
        }
        $user->setClient($client);

        $errors = $validator->validate($user);

        if (count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new JsonResponse(
                $errors,
                400,
                ['Content-Type' => 'application/json'],
                true
            );
        }

        $manager->persist($user);
        $manager->flush();

        $data = [
            'message' => "L'utilisateur a bien été créé",
            'user' => $user
        ];

        $context = SerializationContext::create()->setGroups(array('show'));
        $data = $serializer->serialize($data, 'json', $context);

        return new JsonResponse($data, Response::HTTP_CREATED, [], true);
    }

    /** Update an user (partial update authorized)
    * @Route("/users/{id}", name="update", methods={"PATCH"})
    * @SWG\Parameter(
    *   name="User",
    *   description="Fields to provide to update an User",
    *   in="body",
    *   required=true,
    *   type="string",
    *   @SWG\Schema(
    *       title="User fields",
    *       required={"firstname", "lastname", "email"},
    *       @SWG\Property(property="firstname", type="string", example="Jane"),
    *       @SWG\Property(property="lastname", type="string", example="Doe"),
    *       @SWG\Property(property="email", type="string", example="jane.doe@promophones.fr")
    *   )
    * )
    * @SWG\Response(
    *     response=200,
    *     description="OK",
    *     @SWG\Schema(
    *         type="array",
    *         @SWG\Items(ref=@Model(type=User::class))
    *     )
    * )
    * @SWG\Response(
    *     response=400,
    *     description="BAD REQUEST"
    * )
    * @SWG\Response(
    *     response=401,
    *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
    * )
    * @SWG\Response(
    *     response=403,
    *     description="FORBIDDEN"
    * )
    * @SWG\Response(
    *     response=404,
    *     description="NOT FOUND"
    * )
    * @SWG\Tag(name="User")
    * @nSecurity(name="Bearer")
    * @Security(
    *   "user === userToUpdate.getClient() || is_granted('ROLE_ADMIN')",
    *    message="Vous ne pouvez pas modifier un utilisateur qui ne vous appartient pas"
    * )
    * @param User $userToUpdate
    * @param Request $request
    * @param EntityManagerInterface $manager
    * @param UserPasswordEncoderInterface $encoder;
    * @param SerializerInterface $serializer
    * @return JsonResponse
    */
    public function update(User $userToUpdate, Request $request, EntityManagerInterface $manager, FormErrors $formsErrors, SerializerInterface $serializer) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(UserType::class, $userToUpdate);
        $form->submit($data, false);
        
        if ($form->isSubmitted() && !$form->isValid()) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'errors' => $formsErrors->getErrorsFromForm($form)
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }
        
        $manager->flush();
        
        $data = [
            'message' => "L'utilisateur a bien été mis à jour",
            'user' => $userToUpdate
        ];
        
        $context = SerializationContext::create()->setGroups(array('show'));
        $data = $serializer->serialize($data, 'json', $context);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
    * Delete an user
    * @Route("/users/{id}", name="delete", methods={"DELETE"})
    * @SWG\Response(
    *     response=204,
    *     description="NO CONTENT"
    * )
     * @SWG\Response(
     *     response=401,
     *     description="UNAUTHORIZED - JWT Token not found | Expired JWT Token | Invalid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="FORBIDDEN"
     * )
     * @SWG\Response(
     *     response=404,
     *     description="NOT FOUND"
     * )
    * @SWG\Tag(name="User")
    * @Security(
    *   "user === userToDel.getClient() || is_granted('ROLE_ADMIN')",
    *    message="Vous ne pouvez pas supprimer un utilisateur qui ne vous appartient pas"
    * )
    * @param User $userToDel
    * @param EntityManagerInterface $manager
    * @return JsonResponse
    */
    public function delete(User $userToDel, EntityManagerInterface $manager) : JsonResponse
    {
        $manager->remove($userToDel);
        $manager->flush();
        
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
