<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Client;
use Behat\Transliterator\Transliterator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ClientFixtures extends Fixture
{
    public function __construct(ParameterBagInterface $params, UserPasswordEncoderInterface $encoder)
    {
        $this->params = $params;
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create($this->getLocale());

        // Admin user
        $admin = new Client();
        $admin->setName('Armelle')
            ->setEmail('admin@bilemo.fr')
            ->setPassword($this->encoder->encodePassword($admin, 'password'))
            ->setRoles(['ROLE_ADMIN'])
            ->setIsActive(1)
            ->setDateCreate($faker->dateTime('now', 'Europe/Paris'));
            
        $manager->persist($admin);
       
        // Clients
        $clients = [];
        for ($i = 0;$i < 5; $i++) {
            $dateCreate = $faker->dateTimeBetween('2019-03-15 02:00:49', 'Europe/Paris');
            $dateUpdate = $faker->dateTimeBetween($dateCreate, 'now', 'Europe/Paris');
            $client = new Client();
            $name = $faker->lastName;
            $email = strstr($faker->companyEmail, '@', true) .  '@' . Transliterator::unaccent($name) . '.' . $faker->tld();

            //Infos Users
            $client->setName($name)
                ->setEmail(strtolower($email))
                ->setPassword($this->encoder->encodePassword($client, 'password'))
                ->setRoles(['ROLE_USER'])
                ->setIsActive($faker->randomElement([0,1]))
                ->setDateCreate($dateCreate)
                ->setDateUpdate($faker->dateTimeBetween($dateUpdate, 'Europe/Paris'));

            $manager->persist($client);
            $clients[] = $client;
        }
        
        //Users
        $users = [];
        for ($i = 0;$i < 20; $i++) {
            $dateCreate = $faker->dateTimeBetween('2019-03-15 02:00:49', 'Europe/Paris');
            $dateUpdate = $faker->dateTimeBetween($dateCreate, 'now', 'Europe/Paris');
            $user = new User();
            
            //Clients array without Admin
            foreach ($clients as $client) {
                if ($key = array_search('ROLE_ADMIN', $client->getRoles()) !== false) {
                    array_splice($clients, $key, 1);
                }
            }
          
            //Infos Users
            $firstname = $faker->firstName;
            $lastname = $faker->lastName;
            $client = $clients[mt_rand(0, count($clients) - 1)];
            $email = Transliterator::unaccent($firstname . "." . $lastname . "@" . $client->getName() . "." . $faker->tld());
            $user->setFirstName($firstname)
                ->setLastName($lastname)
                ->setEmail(strtolower($email))
                ->setDateCreate($dateCreate)
                ->setDateUpdate($faker->dateTimeBetween($dateUpdate, 'now', 'Europe/Paris'))
                ->setClient($client);
  
            $manager->persist($user);
            $users[] = $user;
        }
        
        $manager->flush();
    }
    
    public function getLocale()
    {
        return $this->params->get('locale');
    }
}
