<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ProductFixtures extends Fixture
{
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create($this->getLocale());
        $desiredFixturesPhone = 10;

        //Products
        $catalog = [
            'Apple' => [
                'iPhone 11 Pro',
                'iPhone 11 Pro Max',
                'iPhone 11',
                'iPhone XR',
                'iPhone XS Max',
                'iPhone',
                'iPhone 8',
            ],
            'Nubia' => [
                'Red Magic 5G',
                'Red Magic 3S',
                'Z20',
                'Z18',
                'X',
            ],
            'Huawei' => [
                'P40 Lite E',
                'Enjoy 10e',
                'Mate Xs',
                'P30',
                'Y7p',
            ],
            'Xiaomi' => [
                'Redmi Note 9 Pro Max',
                'Redmi 8A Dual',
                'Mi 10 Pro',
                'Mi 10',
            ],
            'Vivo' => [
                'V19',
                'Nex 3S 5G',
                'Apex 2020',
                'Z6 5G',
            ],
        ];

        $products = [];

        for ($i = 0; $i < $desiredFixturesPhone; ++$i) {
            $brand = array_rand($catalog);
            $model = $catalog[$brand][mt_rand(0, count($catalog[$brand]) - 1)];

            $dateCreate = $faker->dateTimeBetween('2019-03-15 02:00:49', 'Europe/Paris');

            $product = new Product();
            $product->setBrand(strval($brand))
                    ->setModel($model)
                    ->setColor($faker->safeColorName())
                    ->setStock($faker->numberBetween(0, 50))
                    ->setDateRelease($faker->dateTimeBetween('-3 years', 'now', 'Europe/Paris'))
                    ->setDateCreate($dateCreate)
                    ->setReference($faker->ean8())
                    ->setPrice($faker->randomFloat(2, 199, 1529))
                    ->setCurrency('EUR')
                    ->setDescription($faker->realText($faker->numberBetween(100, 200)));

            $manager->persist($product);
            $products[] = $product;

            if (false !== ($key = array_search($model, $catalog[$brand]))) {
                array_splice($catalog[$brand], $key, 1);
            }
        }

        $manager->flush();
    }

    public function getLocale()
    {
        return $this->params->get('locale');
    }
}
