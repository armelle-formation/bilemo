<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimeStampableTrait;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *  fields={"brand", "model"},
 *  errorPath="model",
 *  message="Ce model est déjà existant pour cette marque."
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "products_show",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true,
 *      )
 * )
 * @Hateoas\Relation(
 *      "update",
 *      href = @Hateoas\Route(
 *          "products_update",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *         excludeIf = "expr(not is_granted('ROLE_ADMIN'))"
 *      )
 * )
 * @Hateoas\Relation(
 *      "delete",
 *      href = @Hateoas\Route(
 *          "products_delete",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true
 *      ),
 *      exclusion = @Hateoas\Exclusion(
 *         excludeIf = "expr(not is_granted('ROLE_ADMIN'))"
 *      )
 * )
 */
class Product
{
    use TimeStampableTrait;
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"show"})
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Default", "show", "write"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 3,
     *     max = 25
     * )
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Default", "show", "write"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     max = 255
     * )
     *
     */
    private $model;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"show", "write"})
     */
    private $color;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Positive
     * @Groups({"show", "write"})
     */
    private $stock;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"show", "write"})
     * @Type("DateTime<'Y-m-d'>")
     * @Assert\LessThan(
     *  value = "today UTC",
     *  message = "La date de commercialisation doit être antérieure au {{ compared_value }}"
     * )
     */
    protected $dateRelease;
 
    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Default", "show", "write"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     max = 15,
     *     maxMessage = "La référence ne doit pas faire plus de {{ limit }} caractères."
     * )
     */
    private $reference;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"show", "write"})
     * @Assert\Regex(
     *     pattern="/^[0-9]+(\.[0-9]{1,2})?$/",
     *     match="true",
     *     message = "Le prix n'est pas valide"
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"show", "write"})
     * @Assert\Currency(
     * message = "Cette devise n'est pas une devise valide reconnue par la norme ISO-4217")
     * @Assert\Length(
     *     max = 3,
     *     maxMessage = "La devise ne doit pas faire plus de {{ limit }} caractères (format ISO-4217)."
     * )
     */
    private $currency;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"show", "write"})
     * @Assert\Length(
     *     min = 3,
     *     max = 255
     * )
     */
    private $description;

    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(?int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getDateRelease(): ?\DateTimeInterface
    {
        return $this->dateRelease;
    }

    public function setDateRelease(?\DateTimeInterface $dateRelease): self
    {
        $this->dateRelease = $dateRelease;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @Assert\IsTrue(message="Vous devez préciser la devise du prix que vous donnez")
     */
    public function hasCurrency()
    {
        if (!empty($this->price)) {
            return (!empty($this->currency));
        }
        return true;
    }

    /**
     * @Assert\IsTrue(message="Si vous donnez une devise, vous devez préciser un prix")
     */
    public function hasPrice()
    {
        if (!empty($this->currency)) {
            return (!empty($this->price));
        }
        return true;
    }
}
