<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimeStampableTrait;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *      fields="email",
 *      message="Un utilisateur avec le même email existe déjà dans la liste des utilisateurs."
 * )
 * @UniqueEntity(
 *     fields={"firstname", "lastname"},
 *     errorPath="lastname",
 *     message="Un utilisateur avec le même nom et le même prénom existe déjà dans la liste des utilisateurs."
 * )
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "users_show",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true,
 *      )
 * )
* @Hateoas\Relation(
 *      "update",
 *      href = @Hateoas\Route(
 *          "users_update",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true
 *      )
 * )
 * @Hateoas\Relation(
 *      "delete",
 *      href = @Hateoas\Route(
 *          "users_delete",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true
 *      )
 * )
 * @Hateoas\Relation(
 *     "client",
 *     embedded = @Hateoas\Embedded("expr(object.getClient())"),
 *     exclusion = @Hateoas\Exclusion(
 *          excludeIf = "expr(not is_granted('ROLE_ADMIN'))",
 *          groups = "embeddedUsers"
 *      )
 * )
 */
class User
{
    use TimeStampableTrait;
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Expose
     * @Groups({"show"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Expose
     * @Groups({"Default", "show", "write"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 3,
     *     max = 50
     * )
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Expose
     * @Groups({"Default", "show", "write"})
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 3,
     *     max = 50
     * )
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Expose
     * @Groups({"show", "write"})
     * @Assert\NotBlank
     * @Assert\Email()
     * @Assert\Length(
     *     max = 255
     * )
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     * @Expose
     * @Groups({"show","embeddedClient"})
     */
    private $client;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }
}
