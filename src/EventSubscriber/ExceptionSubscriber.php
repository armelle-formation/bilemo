<?php

namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private $environment;
    
    public function __construct(KernelInterface $kernel)
    {
        $this->environment = $kernel->getEnvironment();
    }
    
    public function onKernelException(ExceptionEvent $event)
    {
        $request = $event->getRequest();
        $exception = $event->getThrowable();

        if (method_exists($exception, 'getStatusCode')) {
            $code = $exception->getStatusCode();
        } else {
            $code = $exception->getCode() === 0 ? 500 : $exception->getCode();
        }

        if ($request->getRequestFormat() == 'json' || $code == 405) {

            $message = "Une erreur est intervenue. merci de réessayer ultérieurement";

            // Ressource not found
            if ($exception instanceof NotFoundHttpException) {
                $message = 'Resource not found';
            }
        
            // Access Denied
            if ($exception instanceof AccessDeniedHttpException) {
                $message = 'Access denied';
            }
            
            if($exception instanceof MethodNotAllowedHttpException ){
                $message = 'Method not allowed';
            }

            if ($this->environment === "dev") {
                $message = $exception->getMessage();
            }
            
            $data = [
                'status' => $code,
                'message' => $message
            ];

            $response = new JsonResponse($data);
            $event->setResponse($response);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }
}
