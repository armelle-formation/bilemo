<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'brand',
                TextType::class,
                ['required' => false]
            )
            ->add(
                'model',
                TextType::class,
                ['required' => false]
            )
            ->add(
                'color',
                TextType::class,
                ['required' => false]
            )
            ->add(
                'stock',
                NumberType::class,
                [
                    'required' => false,
                    'scale' => 1
                ]
            )
            ->add(
                'dateRelease',
                DateType::class,
                [
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'html5' => false
                ]
            )
            ->add(
                'reference',
                TextType::class,
                ['required' => false]
            )
            ->add(
                'price',
                MoneyType::class,
                [
                    'required' => false,
                    'currency' => false,
                    'scale' => 2,
                    'invalid_message' => "Le prix n'est pas valide"
                ]
            )
            ->add(
                'currency',
                TextType::class,
                ['required' => false]
            )
            ->add(
                'description',
                TextType::class,
                ['required' => false]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'csrf_protection' => false
        ]);
    }
}
