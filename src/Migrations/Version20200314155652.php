<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200314155652 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product (
            id INT AUTO_INCREMENT NOT NULL, 
            brand VARCHAR(255) NOT NULL, 
            model VARCHAR(255) NOT NULL, 
            color VARCHAR(255), 
            stock INT DEFAULT NULL, 
            date_release DATETIME DEFAULT NULL, 
            date_create DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            date_update DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            reference VARCHAR(255) NOT NULL, 
            price DOUBLE PRECISION, 
            currency VARCHAR(255), 
            description LONGTEXT DEFAULT NULL, 
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product');
    }
}
