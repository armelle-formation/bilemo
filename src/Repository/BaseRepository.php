<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * Custom Repository base class containing specifics methodes to this application.
 * To use in your repository class, change the extended class by BaseRepository
 */
class BaseRepository extends ServiceEntityRepository
{
    
    /**
     * Count items in this repository
     *
     * @return array
     */
    public function findAllByOrder($arguments)
    {
        return $this->findBy(
            array(),
            array($arguments['orderby'] => $arguments['sort']),
            $arguments['limit'],
            $arguments['offset']
        );
    }
}
