<?php

namespace App\Repository;

use App\Entity\Client;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends BaseRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof Client) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * Get Clients list without admin (default paged by 10)
     * @param array $arguments
     * @return Client[] Returns an array of Client objects
     */
    public function findAllClientsWithoutAdmin(array $arguments)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.roles LIKE :role')
            ->setParameter('role', '%ROLE_USER%')
            ->setMaxResults($arguments['limit'])
            ->setFirstResult($arguments['offset'])
            ->orderBy('c.' . $arguments['orderby'], $arguments['sort'])
            ->getQuery()
            ->getResult()
        ;
    }
}
