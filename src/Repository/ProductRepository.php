<?php

namespace App\Repository;

use App\Entity\Product;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * Search products by criteria (default paged by 10):
     * - search by brand
     * - search by model
     * - search by keyword on reference, currency or description
     * - with limit and offset, sort and order by
     * @param array $arguments
     * @return Product[] Returns an array of Product objects
     */
    public function findAllByCriterias(array $arguments)
    {
        $query = $this->createQueryBuilder('p')
                      ->setMaxResults($arguments['limit'])
                      ->setFirstResult($arguments['offset'])
                      ->orderBy('p.' . $arguments['orderby'], $arguments['sort']);

        // Search by Brand
        if (array_key_exists('brand', $arguments) && strlen($arguments['brand'])) {
            $query->andWhere('lower(p.brand) LIKE :brand')
                  ->setParameter('brand', '%' . strtolower($arguments['brand']) .'%');
        }

        // Search by Brand
        if (array_key_exists('model', $arguments) && strlen($arguments['model'])) {
            $query->andWhere('lower(p.model) LIKE :model')
                  ->setParameter('model', '%' . strtolower($arguments['model']) .'%');
        }

        // Search by Keyword on ref, description or currency
        if (array_key_exists('keyword', $arguments) && strlen($arguments['keyword'])) {
            $query->andWhere('lower(p.reference) LIKE :keyword OR lower(p.currency) LIKE :keyword OR lower(p.description) LIKE :keyword')
                  ->setParameter('keyword', '%' . strtolower($arguments['keyword']) .'%');
        }

        return $query->getQuery()->getResult();
    }
}
