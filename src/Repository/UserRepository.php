<?php

namespace App\Repository;

use App\Entity\User;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Return a list of users for a client
     * @param array $arguments
     * @return User[] Returns an array of User objects
     */
    public function findAllUsersByClient(array $arguments)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.client = :client')
            ->setParameter('client', $arguments['client'])
            ->orderBy('u.' . $arguments['orderby'], $arguments['sort'])
            ->setMaxResults($arguments['limit'])
            ->setFirstResult($arguments['offset'])
            ->getQuery()
            ->getResult();
    }
}
