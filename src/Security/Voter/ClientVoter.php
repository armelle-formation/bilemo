<?php

namespace App\Security\Voter;

use App\Entity\Client;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ClientVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['DELETE','UPDATE'])
            && $subject instanceof \App\Entity\Client;
    }

    protected function voteOnAttribute($attribute, $client, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case 'DELETE':
                if ($client != $user) {
                    return true;
                }
            break;
            case 'UPDATE':
                if (($client == $user && $client->getIsActive()) || in_array('ROLE_ADMIN', $user->getRoles())) {
                    return true;
                }
            break;
            default:
                return false;
            break;
        }
    }
}
