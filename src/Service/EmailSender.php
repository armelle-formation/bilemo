<?php

namespace App\Service;

use Exception;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;

/**
 * Class EmailSender
 */
class EmailSender
{
    private $mailer;

    private $twig;

    /**
     * EmailSender constructor.
     * @param Swift_Mailer $mailer
     * @param Environment $twig
     */
    public function __construct(Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     *  Send a mail with swiftmailer with a twig template
     *
     * @param string $subject
     * @param [type] $content
     * @param string $recipient
     * @param string $sender
     * @return int
     */
    public function sendMessage(string $subject, $content, string $emailRecipient, string $emailSender)
    {
        $sender = [$emailSender => 'Bilemo'];
        $return = [
            "success" => false
        ];

        try {
            $response = $this->mailer->send(
                (new Swift_Message($subject))
                ->setFrom($sender)
                ->setReplyTo($sender)
                ->setTo($emailRecipient)
                ->setBody($content)
                ->setContentType("text/html")
            );

            if ($response) {
                $return = [
                    "success" => true
                ];
            }
        } catch (Exception $e) {
            dump($e);
            die();
        }

        return $return;
    }
}
