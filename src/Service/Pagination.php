<?php

namespace App\Service;

use Exception;
use Doctrine\ORM\EntityManagerInterface;

class Pagination
{

    /**
     * Entity to be paged
     *
     * @var string
     */
    private $entityClass;

    /**
     * The number of records to retrieve
     *
     * @var integer
     */
    private $limit;

    /**
     * Current page
     *
     * @var integer
     */
    private $currentPage = 1;

    /**
     * Doctrine manager to find the entityClass repository
     *
     * @var ObjectManager
     */
    private $manager;

    /**
     * SortBy
     *
     * @var string
     */
    private $sort;

    /**
     * Order sort
     *
     * @var string
     */
    private $orderby;

    /**
     * Method of repo
     *
     * @var string
     */
    private $method = 'findAllByOrder';


    /**
     * Arguments for repo Method and/or for render
     *
     * @var array
     */
    private $arguments;


    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;
        return $this;
    }

    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setPage(int $page): self
    {
        $this->currentPage = $page;
        return $this;
    }

    public function getPage(): int
    {
        return $this->currentPage;
    }

    public function setSort(string $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getSort(): string
    {
        return $this->sort;
    }

    public function setOrderBy(string $orderby): self
    {
        $this->orderby = $orderby;

        return $this;
    }

    public function getOrderBy(): string
    {
        return $this->orderby;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setArguments(array $arguments): self
    {
        $this->arguments = $arguments;

        return $this;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }
    
    /**
     * Constructor
     * @param ObjectManager $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->limit = getenv('LIMIT');
        $this->orderby = getenv('ORDERBY');
        $this->sort = getenv('SORT');
    }

    /**
     * Allows to retrieve paginated data for a specific entity
     * @throws Exception if the entityClass property is not passed
     * @return array
     */
    public function getData()
    {
        if (empty($this->entityClass)) {
            throw new \Exception("Aucune entité spécifiée ! Utilisez la méthode setEntityClass() de votre objet PaginationService");
        }
        
        $args = [
            'sort' => $this->sort,
            'orderby' => $this->orderby
        ];

        $offset = $this->currentPage * $this->limit - $this->limit;
        $args['limit'] = $this->limit;
        $args['offset'] = $offset;
        
        if (!empty($this->arguments)) {
            $args = array_merge_recursive(
                $args,
                $this->arguments
            );
        }
        
        return $this->manager
            ->getRepository($this->entityClass)
            ->{$this->method}($args);
    }

    /**
     * Allows to retrieve the number of pages that exist on a particular entity
     * @throws Exception if the entityClass property is not passed
     * @return int
     */
    public function getPages(): int
    {
        if (empty($this->entityClass)) {
            throw new \Exception("Aucune entité spécifiée ! Utilisez la méthode setEntityClass() de votre objet PaginationService");
        }
        $total = count($this->getData(true));
        return ceil($total / $this->limit);
    }
}
