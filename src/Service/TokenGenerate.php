<?php

namespace App\Service;

use App\Entity\Client;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

class TokenGenerate
{

    /**
     * Generate a JWT Token
     */
    public function generateTokenForUser(JWTEncoderInterface $jwtEncoder, Client $client, int $nbMinutsValidity = 30)
    {
        //Generate token
        $issuedAt = time();
        $payload = array(
            'id' => $client->getId(),
            'email' => $client->getEmail(),
            'iat' => $issuedAt,
            'exp' => $issuedAt + (60 * $nbMinutsValidity)
        );

        return $jwtEncoder->encode($payload);
    }
}
